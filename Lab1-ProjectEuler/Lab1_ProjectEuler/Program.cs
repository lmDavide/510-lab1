﻿using System;

namespace Lab1_ProjectEuler{
    class Program{
        static void Main(string[] args){

            string[] fileArray = System.IO.File.ReadAllLines("Resources//euler11.txt");

            int fileArrayLength = fileArray.Length;

            int[,] numsArray = new int[fileArrayLength,fileArrayLength];

            for(int i = 0; i < fileArrayLength; i++){

                string[] gridLine = fileArray[i].Split(" ");

                for(int j = 0; j < gridLine.Length; j++){

                    numsArray[i,j] = int.Parse(gridLine[j]);

                }

            }

            int xLength = numsArray.GetLength(0);
            int yLength = numsArray.GetLength(1);

            Console.WriteLine("X Length: " + xLength);
            Console.WriteLine("Y Length: " + yLength);
            
            Console.WriteLine("");

            Console.WriteLine("Grid: ");

            for(int i = 0; i < xLength; i++){

                for(int j = 0; j < yLength; j++){

                    Console.Write(numsArray[i,j] + " ");
                }

                Console.WriteLine("");
            }
            
            
            

        }
    }
}
